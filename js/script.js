$('body').on('submit', 'form', function (e) {
	e.preventDefault();
	alert($(this).find('input[name="value"]').val());
});

// Evento de envio de form
// Funciona com 'button[type="submit"]', 'input[type="submit"]', '.submit'
//Basta adicionar no botão ou input -- for="Formulário-Que-Vai-Enviar"
$('body').on('click', 'button[type="submit"], input[type="submit"], .submit', function () {
	var id = this.getAttribute('for');
	var form = $('form[id="' + id + '"]');
	if (form.size() > 0) {
		form.submit();
	}
});